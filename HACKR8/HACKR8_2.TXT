            -= H A C K E R S =-

          Issue #8,  File #2 of 9

               Letters


>From matt@matt.net.house.auSun Apr 21 18:41:18 1996

Date: Wed, 17 Apr 1996 19:09:28 +1000 (EST)
From: Matty <matt@matt.net.house.au>
To: Mrs3691@hertz.njit.edu
Subject: Handy Hack


G'day.. just started reading HACKERS #8 (finally got around to getting
it!) and thought I may as well make a fool of myself and contribute this
hack by a friend and I.  It's not very exciting, and has nothing to do
with breaking into secret government computers or anything - but we were
proud of ourselves.

  This was two years ago - when the lab that held the Sun workstations was
restricted to a few students 9and not open like it has been since they
stuck the colour linux boxes into another lab - anyway)
There was an aging line printer... 132 columns or so... extremely noisy
and usually having reams and reams of paper in a complete mess on the
out-tray bin and all over the floor.  (it's been replaced by a laser now..
but I continue)
  The problem was, there were some morons in the lab who were either
blind, stupid, or had difficulty reading the large sign above the printer
that we had made saying "THIS PRINTER DOES NOT PRINT POSTSCRIPT" - which
it didn't.  Every time a postscript file came through, the printer would
happily churn out literally hundreds of pages of gibberish - wasting both
paper and ink - and being a general nuisance to those of us who had
important things to print out.  One night my friend and I had important
things to print, but some lusers had postscript jobs queued ahead of us.
Anyway, it was typical programmers time (like about midnight or 1am) so
the Big Chief wasn't around to log in as root and nuke the jobs.
  So anyway, after the usual "you dipshit" letters to the offenders, we
started thinking how we could get our jobs to go through.
Well, as usual, the printer was out of paper anyway.  So I went on the
hike to the Computer Services Centre (from our faculty) with my large
backpack, and returned with around 3 inches of fanfold paper.  The
advantage of the printer room (which is now gone - but basically it was a
partitioned-off section of a general computer lab) was that the security
camera was blocked by the partition - hence I could rip off as much paper
& stuff as I liked.  The fake job I printed out was discarded in the usual
manner (banner & login name ripped into various segments and disposed in
random bins along the way out along with a few cusses about the printer
screwing up again) and I hiked back to the faculty with the paper.
  First off, we took the ribbon out (no sense wasting the ink - it was
almost dry anyway) and then tried taking the paper out but of course the
printer had built-in detectors and wouldn't print.  So we tried the
looping paper trick, but it started strangling the tractor.
Next we tried getting a long strand of holey bits and looping that around
- it failed due to a second paper-detector.
So two loops were made - and after the long wait for the glue to dry we
put them in - and it worked for a bit - then the loops jumped out.  So we
cut them up tight enough to not jump but loose enough not to rip (it was
really crappy paper that would rip if you looked at it hard enough) -
waited again for the glue to dry - and set it off again.  Worked
perfectly - we jammed a bit of paper over the tractor so it wouldn't get
damaged by the hammers, and hour or so later the crap stuff was out of the
queue and we could print our stuff.
While we waited my friend wrote up a short spiel about how to clear the
print buffer on the printer and how to defeat postscript jobs - this was
pinned on the noticeboard above the printer where it stayed until we took
it down when the line printer was replaced by a laser.
The tech. staff thought it was cool - we had a few compliments from others
who used the lab and experienced the same problems we did - and yeah we
were pretty proud.

Well that's it.  Not a great hack, but still, a hack.

ObLittleHack:
Dialling 199 on a Telecom Commander PABX actually rings _every_ extension
on the system.  Kinda fun in a big shop where you spin some crap to a
clueless salesperson who wanders off to find out and you reach over & grab
the phone, dial, hang up, look innocent, and wait for the joyful tones of
an entire PABX ringing - then the look on the faces of everyone who rushes
to pick up the phone, gives the standard welcome speech "Hello, Fred
Bloggs Nose Picking service, Jane Doe speaking; how can I help you?" and
gets the annoying *beeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeep* in the ear.
(Australia only - I assume the numbers & stuff differ overseas)

matt@matt.net.house.au (not on DNS)
tp943845@ise.canberra.edu.au (on DNS)
-----BEGIN GEEK CODE BLOCK-----
Version: 3.12
GIT/GMU/GO d- s++:- a-- C+++ UL++++(S+) P--- L++++>$ E- W+++(--) N+(++) !o
K- w--- O M-- V--(!V) PS+ PE++ Y+ PGP t+ 5++ X+ !R tv--- b+ DI(+) D++ G--

e>++ d++ r--->+++ !y+

------END GEEK CODE BLOCK------

-------------------------------------------------------------------------------

>From matt@matt.net.house.auSun Apr 21 18:41:27 1996

Date: Wed, 17 Apr 1996 19:58:35 +1000 (EST)
From: Matty <matt@matt.net.house.au>
To: Mrs3691@hertz.njit.edu
Subject: Another Hack


Oh yeah... forgot to add this one:

Lacking sufficient ethernet cabling and cards at the time, we were unable
to set up net.house properly - however with a few null-modem cables and
SLiRP (http://blitzen.canberra.edu.au/slirp/) we finally got the place
nettable.  The link looked like this

                                        INTERNET
                                           |
                                       128K_ISDN
                                           |
                                    /~~~~~~~~~~~~~~\
                                    |AARNet gateway|
                                    \______________/
                                          |
                                         eth
                                          |
/~~~~~~~\       /~~~~~~~~~~~\       /~~~~~~~~~~~\       /~~~~~\
|blitzen|--eth--|ise-gateway|--eth--|csc-gateway|--eth--|annex|
\_______/       \___________/       \___________/       \_____/
                                                           I
                                                         modem
                                                           I
                                                        (telco)
                                                           I
                                                       my-modem
                                                           I
           /~~~~~~~~\          /~~~~~~\          /~~~~~~~~~~~~~~\
           |neil-mac|==serial==|smoopc|==serial==|matt.net.house|
           \________/          \______/          \______________/

In order, my PC runs SLiRP off blitzen through the annex (they haven't
given proper (C)SLIP/PPP to students yet) Smoo runs slirp off my PC, and
Neil runs slirp off Smoo's PC.  So when you look at it, Neil's Mac was
faking it was Smoo's PC which was faking it was my PC which was faking it
was blitzen.canberra.edu.au *phew!*
My poor little 28.8K modem was pretty busy keeping up with it all, but
the link worked (albeit slow at times - especially the Mac - hehe)
When Spakman moved in, the link got worse - it was the same to my pc, then
we had spak's pc slirping off mine and his laptop slirping off his pc, and
Neil's mac slirping off my other serial port.  A nightmare of cables and
not much say in whose PC went where on the desk (cable length limitations)
Now I have my ethernet card back, and neil has a new ethernet-able mac, so
net.house is fully coax-ed and there's just the one slirp link faking
PPP from my pc to blitzen - and I IP-forward and proxyarp the rest of the
house.  But that serial==serial==serial==serial slirp connection to get
every computer in the house on the net has to be one of the better hacks
we've done this year.

Again, not a 'secret government documents' - kind of hack, but I've been
out of that scene for a while...

matt@matt.net.house.au (not on DNS)
-----BEGIN GEEK CODE BLOCK-----
Version: 3.12
GIT/GMU/GO d- s++:- a-- C+++ UL++++(S+) P--- L++++>$ E- W+++(--) N+(++) !o
K- w--- O M-- V--(!V) PS+ PE++ Y+ PGP t+ 5++ X+ !R tv--- b+ DI(+) D++ G--

e>++ d++ r--->+++ !y+

------END GEEK CODE BLOCK------

-------------------------------------------------------------------------------

>From matt@matt.net.house.auSun Apr 21 18:41:32 1996

Date: Wed, 17 Apr 1996 20:25:30 +1000 (EST)
From: Matty <matt@matt.net.house.au>
To: mrs3691@hertz.njit.edu
Subject: Australian ANI numbers & stuff


They say there's two types of users - those who read everything first and
then send the mail, and those (like me) who just keep on mailing and
mailing until everything is read & sent :)

Anyway, I'm on to the letters section now.  I don't know about that 1114
and 1115 that some guy/gal wrote in - it could be true but it sounds kinda
fake.  The number I know has always been 19123.  HOWEVER (I just had a
brief flash of insight) the 111x stuff _could_ be for a pay phone - I'll
have to test that one out.  19123 does _not_ work on pay phones (it says
"No information to identify telephone number" in this cool robotic voice)
 - works on normal house phones though (and I assume a PABX or other
non-pay-phone phone)

New info:  dialling "12711" will identify which service carrier you're
using for long distance calls, and how to use a different carrier.  I
guess it's of some use to someone somewhere in Australia :)

Apologies for my 3-message spam - I really should learn to collate all my
thoughts first but I'm a spontaneous kind of person :)

matt@matt.net.house.au (not on DNS)
-----BEGIN GEEK CODE BLOCK-----
Version: 3.12
GIT/GMU/GO d- s++:- a-- C+++ UL++++(S+) P--- L++++>$ E- W+++(--) N+(++) !o
K- w--- O M-- V--(!V) PS+ PE++ Y+ PGP t+ 5++ X+ !R tv--- b+ DI(+) D++ G--

e>++ d++ r--->+++ !y+

------END GEEK CODE BLOCK------

[ Exactly the kind of hacks I was looking for...anybody can hack into
government computers, but what the hell do you do when the printer is out
of paper?  Definetly cool hacks.  You win the Hack of the Month award, send
me your snail mail address and I'll mail it out to you. ]

-------------------------------------------------------------------------------

>From jmfriedman@chop.isca.uiowa.eduSat Mar 30 18:27:28 1996

Date: Thu Mar 28 19:07:34 1996
From: jonathon friedman <jmfriedman@chop.isca.uiowa.edu>
To: mrs3691@hertz.njit.edu
Subject: HACKERS 7

I would like to respond to  the 13 year old who wants to learn how to hack.  I
would not recomend it, or at least actually doing it espieccially if you are a
"newbie". You will probably ask why should you tell me what to do,w ell, I am
13 years old as well, and I used a rlogin bug to get in to a computer at our
university (when i was 12) and I didn'at do anything, though Ic oudl have bu t
I was caught they didn't do anything like taking me to court but if I had done
something I could have been, anywyas the moral, Don't hack, especially if youd
on't know wha tthe hell you are doing.

--Jon
---
Wise man say the meaning of life is "Life is like.....Ever seen forest gump?"

[ Good advice, despite the typos...in my opinion, the real hacks are those
like the first two letters.  Anyone can run an 8lgm script.  Levy's version
of Hackers is what it is really all about. ]

-------------------------------------------------------------------------------

>From turgjb@ee.mcgill.caSat Mar 30 18:43:39 1996

Date: Sat, 30 Mar 1996 14:48:22 -0500 (EST)
From: Turgeon Jean-Benoit <turgjb@ee.mcgill.ca>
To: mrs3691@hertz.njit.edu
Subject: Hackers subscription

I read the latest edition of Hackers. Its a great zine!
Sysadmins at our lab are quite paranoids about hackers, they feel the network
is threathened by hackers every minute and even look for any file containing
the word "hack" in all users home dirs. Anyway, hiding ourselves is not
the way to go. So to end this story could you add my address to the
subscribtion list.

Thanks                JBT

[ The same thing is done on the computers at my campus.  If anyone out there
is currently in college, watch the names of the files you leave lying around. ]

-------------------------------------------------------------------------------

>From arcane@texnet.net Thu Oct 10 19:58:01 1996

Date: Wed, 04 Sep 1996 09:24:35 -0500
From: Brian Thomas <arcane@texnet.net>
To: mrs3691@hertz.njit.edu
Subject: phone number decyphering

I ran across issue six from 96 and saw the article on dialing the ANI code
to determine the number you are at.  In the article wyle@max.tiac.net wrote
about a three digit code that will give you the phone number for any phone
in an area code.  In our area code (817) I have found that the numbers are
actually based on the prefix and not the area code.  For example
817-741-#### would dial 974 but 817-752-#### would dial 972.  For anyone
that tried to find a number based on this code and failed you might try
again and use the numbers around it because of a difference in your prefix.

-------------------------------------------------------------------------------

>From gauravm@giasbma.vsnl.net.in Thu Oct 10 20:01:48 1996

Date: Mon, 30 Sep 1996 19:09:53 +0530 (IST)
From: GAURAV MARBALLI <gauravm@giasbma.vsnl.net.in>
To: mrs3691@hertz.njit.edu
Subject: Very tightly secured UNIX system

Hi! My name is WeByte from Mumbai, India. Our ISP gives us a UNIX based
Lynx browser for the Internet (Lynx version 1.0). We get access to the
UNIX prompt, but it is a Restricted Korn Shell ('coz it says rksh:
everytime). Earlier security was not so tight and so me and my friend used
to Cracker-Jack the password file, to get the passwords for the more
expensive TCP/IP accounts. But not anymore man!! Security is so bad, we
aren't allowed to use even "ls -la" forget "cd". So help me man. I tried
methods suggested by other hackers like "shelling" out of "vi" and stuff
like that, but man, we do not even have "vi". We use PICO. So help me
man!! I want that passwd file. (Remember I am a rookie) Bye for now! and
Thanx!!

[ Another testament to the horror of not leaving back doors. ]

*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *
