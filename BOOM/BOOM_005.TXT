
               Things That Go 'BOOM' and Other Stuff That Rulez
                                  Issue Five
                              
                                  Written by
                             --------------------
                                   Cerberus
                                  Count Zero 
                             --------------------
                              Shroud of Deception
                              Gut Shoveler (Gutz)
                                 616.775.2945
                             --------------------
                                    4-16-94
                             --------------------

WARNING: Don't try this at home.  If your stupid enough to try any of this
shit, we're not responsible.  We aren't gonna pay your hospital bills because 
you blew off your thumb.  We'll just laugh at you. WE AREN'T RESPONSIBLE FOR 
ANY DAMAGES CASUED BY USE OF ANY OF THE MATERIAL PRESENTED IN THIS FILE.
                                                                        
In this issue we will be explaining how to make simple explosives and    
projectiles.

Gas bomb: 
  Yes, now you the average law abiding citizen can create a gas bomb.  But,
  just because you can doesn't mean you should.  Okay now that we've warned
  you, have at it.
 Ingredients:
  some good duct tape
  1 plastic botlle 
  enough gasoline to fill the above bottle
  1 wick (one made with C&C Hellfire Mixture recommended.  See issue #3)
  some rubber cement 
 Directions:
  Now that you have all that shit, you fill the bottle with gas, duct tape 
  it to the side of a building/car/human being/whatever (taping is optional). 
  Then rubber cement the wick to the side of the bottle and lite.  The wick 
  will burn to the rubber cement.  The rubber cement will burn through the 
  bottle and ignite the gas..........BOOOM!
 Notes: 
  If you let rubber cement harden, your a dumb-ass.

Small rocket projectile: 
  Thanks to this newsletter, now you can build your very own personnal rocket
  launcher.  Make the neighbors jealous, then blow the shit out of them.  Of
  course, we don't recommend blowing up your neighbors.... we just think it'd
  be kewl.
 Ingredients:
  1 PVC pipe as thick as you can get it, 3-6 inches in diameter
  1 "Mosquito" type model rocket (or homemade rocket)
  1 AA size multi-stage rocket engine
  1 real fuse, the longer the better
  1 jig saw or cicular saw
  1 reinforcment for the PVC pipe
 Directions:
  Once you have gathered all the necassary items listed above follow these 
  simple steps to create your own personnal rocket launcher.
  1. After buying the model rocket put it together and set it aside, you 
     won't need it for a while.
  2. Take the PVC pipe and cut 3 lines all the way to the bottom but don't 
     cut the pipe into three parts.  These slits will gide the rocket as it 
     exits the pipe.
  3. Drop the rocket down into the pipe make sure the fin's of the rocket 
     slide down into the slits you cut.
  4. Some how you'll have to back the end of the PVC pipe and drill a hole 
     in the middle then stick the fuse in through the hole and the hole in 
     the rocket engine.
  5. Light the fuse and hold on your shoulder (its best if you have some 
     thing to set the pipe on instead of your shoulder).
 Note:
  The best way to rienforce the pipe is to cement the outside walls and then  
  cut the pipe if you are not willing to do this don't even bother.
  You can get the "Misquito" rocket and the fuse from your nearby hobby shop.
  For a complete explanation of model rocket engines, read Things that go 
  'BOOM' - issue 4.
 Pretty Diagram:
                      ++++++++++++++++++++++++++++++++++
                      +backing of pipe-> |     fuse    +
                      +                  |     |       +
                      +                  |    \|/      +
                      +        hole -->  -----------   +
                      +                  |             +
                      +backing of pipe-> |             +
                      +                  |             +
                      ++++++++++++++++++++++++++++++++++

The Amazing Fireball Shooting Rod:
  Dazzle your friends while burning of their eyelashes with this amazing
  rod.
 Ingredients:
  1 thick METAL pipe with METAL backing
  lots and lots of gun powder
  sevral cotton balls
  petroleum jelly (common vasoline, you know the lubricant)
  metal rod that fits in the in the above pipe
  wick (we recommend using a C&C Hellfire Wick)
  1 lighter
 Directions:
  Once you have every thing, take the gun powder and pour as much or as 
  little as you want in the pipe.  Pack the gun powder, but for god sake's 
  man be careful.  Take the cotton balls (as many or as little as you want), 
  and cover them with the petroleum jelly.  Then pack them in real good.  
  Put the wick in far enough so when it burns down it will light the cotton 
  balls too.  The cotton balls will light the gun powder and hurl the flaming 
  cotton balls out of the pipe.

Ideas from Watson:
  Okay, Watson bugged us enough so we've given him his own Soapbox.  Don't
  worry, this is only a one-time thing.  Unless of course, you want him
  to do it again (you need help!) and you tell us.  Well, here goes:
   Hey, anybody ever wondered what would happen if you poured a whole lotta
   gasoline in the lake (of course near the swimming area) and then told an
   old guy to throw a cigarette in the water?  <Editor's Note: no Watson,
   we didn't even think about it.>  Well, I did.  I also thought up of lots
   of cool ideas but Cerberus thought most of them were to sick for this 
   newsletter, but here's one mildly fun one.
   Dig a deep pit to catch little woodland creatures (and annoying neighbors 
   too) and like fill it with lots of gasoline.  Then, when they're passing
   out from the fumes pick them up and smash 'em against a tree (works
   best with rabits if ya grab by the ears).  After they seem partially 
   dead (not all the way yet... don't peek to soon) then make sure the gas
   is still wet and light 'em on fire!  Now your probably saying "Watson,
   thats pretty laim we didn't even get to kill them."  <Editor's Note: no
   Watson, we didn't even get this far>  Well sure you didn't kill them but
   by letting them go think how many more you kill.  You see, when they 
   breed they light their mates on fire.  I got the idea from a cockraoch 
   commercial when they take the poison and bring it back to their nests.  
   <Editor's Note: Watson, don't you think the third degree burns would 
   kill them?  I mean really, you're talking about saoking an animal in 
   gas and then lighting them.  Come on man!  They don't light the other 
   animals on fire.  They can't!  They're dead!>  Ummm, okay, yeah, I didn't
   think of that.
  Okay, again I apologize.  Oh by the way, he actually did say all of that.
  No really, I serious... he was actually over here and typed this.

  Beware, further issues to come...

