
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.05
Original issue date: March 5, 1996
Last revised: August 30, 1996
              Information previously in the README was inserted
              into the advisory.

              A complete revision history is at the end of this file.

Topic: Java Implementations Can Allow Connections to an Arbitrary Host
- -----------------------------------------------------------------------------

The CERT Coordination Center has received reports of a vulnerability in
implementations of the Java Applet Security Manager. This vulnerability is
present in the Netscape Navigator 2.0 Java implementation and in Release
1.0 of the Java Developer's Kit from Sun Microsystems, Inc. These
implementations do not correctly implement the policy that an applet may
connect only to the host from which the applet was loaded.

The CERT Coordination Center recommends installing patches from the vendors,
and using the workaround described in Section III until patches can be
installed.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

Although our CA-96.05 CERT advisory does not discuss JavaScript, there have
been a series of recent postings to newsgroups concerning a vulnerability in
the way Netscape Navigator (Version 2.0) supports JavaScript.

As a clarification to our readers, this problem is different from the problem
described in advisory CA-96.05.

Netscape Version 2.01 is now available. This version addresses the Java Applet
Security Manager and the JavaScript problems recently discussed.  For
additional information about these issues and to obtain the new release,
please see:

         http://home.netscape.com/eng/mozilla/2.01/relnotes/


- -----------------------------------------------------------------------------

I.   Description

     There is a serious security problem with the Netscape Navigator 2.0 Java
     implementation. The vulnerability is also present in the Java Developer's
     Kit 1.0 from Sun Microsystems, Inc. The restriction allowing an applet to
     connect only to the host from which it was loaded is not properly
     enforced. This vulnerability, combined with the subversion of the DNS
     system, allows an applet to open a connection to an arbitrary host on the
     Internet. 

     In these Java implementations, the Applet Security Manager allows an
     applet to connect to any of the IP addresses associated with the name
     of the computer from which it came. This is a weaker policy than the
     stated policy and leads to the vulnerability described herein.

II.  Impact

     Java applets can connect to arbitrary hosts on the Internet, including
     those presumed to be previously inaccessible, such as hosts behind a
     firewall. Bugs in any TCP/IP-based network service can then be exploited.
     In addition, services previously thought to be secure by virtue of their
     location behind a firewall can be attacked. 

III. Solution

     To fix this problem, the Applet Security Manager must be more strict
     in deciding which hosts an applet is allowed to connect to. The Java
     system needs to take note of the actual IP address that the applet truly
     came from (getting that numerical address from the applet's packets as
     the applet is being loaded), and thereafter allow the applet to connect
     only to that same numerical address. 

     We urge you to obtain vendor patches as they become available.
     Until you can install the patches that implement the more strict
     applet connection restrictions, you should apply the workarounds
     described in each section below.

     A. Netscape users

        For Netscape Navigator 2.0, use the following URL to learn more about
        the problem and how to download and install a patch:

            http://home.netscape.com/newsref/std/java_security.html 
   
        Until you install the patch, disable Java using the "Security
        Preferences" dialog box.
         

     B. Sun users 

        A patch for Sun's HotJava will be available soon.

        Until you can install the patch, disable applet downloading by
        selecting "Options" then "Security...". In the "Enter desired security
        mode" menu, select the "No access" option.

        In addition, select the "Apply security mode to applet loading" to
        disable applet loading entirely, regardless of the source of the
        applet.  
         

     C. Both Netscape and Sun users
        
        If you operate an HTTP proxy server, you could also disable
        applets by refusing to fetch Java ".class" files. 


- ---------------------------------------------------------------------------
The CERT Coordination Center thanks Drew Dean, Ed Felton, and Dan Wallach of
Princeton University for providing information for this advisory. We thank
Netscape Communications Corporation, especially Jeff Truehaft, and Sun
Microsystems, Inc., especially Marianne Mueller, for their response to this
problem. 
- ---------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact the
CERT staff for more information. 

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce


Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided it
is used for noncommercial purposes and the copyright statement is included.

CERT is a service mark of Carnegie Mellon University.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Aug. 30, 1996  Information previously in the README was inserted into the
               advisory.

Mar. 15, 1996  Introduction - added clarification on JavaScript and pointers to
               Netscape Version 2.01.




-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMiTA+3VP+x0t4w7BAQFdVAQAwHytO8/JOLrJkzdlSLR9crWVigHxMak3
ZC4U7jWsjlB2LG6dhwLg7G/G3ukbftfT6HqkImZFgaF15jVoyALtXEVzx1OYt3GR
QMHU5jx7M4eMuJwDml5DcIWG8FTlFDzmyMrNsLn9iKc6T+UKF4Gi144CgONbWClr
Y/ML45WaCpA=
=jR81
-----END PGP SIGNATURE-----

