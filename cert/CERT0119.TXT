
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.14
Original issue date: July 24, 1996
Last revised:  November 27, 1996
	       Updated Sun patch information. 

              Information previously in the README was inserted into the
               advisory.

              A complete revision history is at the end of this file.

Topic: Vulnerability in rdist
- -----------------------------------------------------------------------------

          *** This advisory supersedes CA-91:20 and CA-94:04. ***

The CERT Coordination Center has received reports that a new vulnerability
in rdist has been found and an exploitation script is widely available.
Current reports indicate that the script works on x86-based versions of the
UNIX Operating System; however, we believe that it would not be difficult to
write variants that work on other instruction sets and configurations.

The CERT/CC Staff recommends following the steps in Section III.A. to
determine if your system is vulnerable and to disable vulnerable programs,
then following your vendor's instructions (Section III.B and Appendix A).
Until you can install a vendor patch, you may want to install a freely
available version of rdist, noted in Section III.C.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

- -----------------------------------------------------------------------------

I.   Description

     The rdist program is a UNIX Operating System utility used to distribute
     files from one host to another. On most systems, rdist is installed as
     set-user-id root, a necessity due to its design. Unfortunately, this
     setting makes it a favorite target for vulnerability investigation.

     A new vulnerability in rdist has been discovered and reported. The
     vulnerability lies in the lookup() subroutine where the value of a
     command line argument is used to overflow the subroutine call stack.
     If that argument is specially crafted with native machine code,
     lookup() returns control to the code added to the call stack instead of
     the subroutine that called lookup(). If, for example, this added code
     uses a member of the exec system call family and names /bin/sh as the
     program to be executed, that shell is then run with set-user-id root
     privileges. No matter what code is added, the code runs with
     set-user-id root privileges.

     An exploitation program, which is circulating on the Internet, takes
     advantage of this vulnerability. While it purports to work only on
     x86-based versions of the UNIX Operating System, variants tuned to other
     instruction sets and configurations are straightforward to write.

II.  Impact

     On unpatched systems, anyone with access to a local account can gain root
     access.

III. Solution

     We urge you to follow the steps in Section A to determine if your system
     is potentially vulnerable and, if it is, to turn off rdist while you
     decide how to proceed.

     If you need the functionality that rdist provides, install a vendor
     patch (Sec. B). Until you can do so, you may want to install a freely
     available version of rdist that does not need to be installed as
     set-user-id root and is, therefore, not susceptible to the exploitation
     described in this advisory (Sec. C).

     A. How to determine if your system is vulnerable

        To determine if a system is vulnerable and to disable the programs
        that are believed to be vulnerable, use the following find command or
        a variant. Consult your local system documentation to determine how to
        tailor the find program on your system.

        You will need to run the find command on each system you maintain
        because the command examines files on the local disk only. Substitute
        the names of your local file systems for FILE_SYSTEM_NAMES in the
        example. Example local file system names are /, /usr, and /var.
        You must do this as root.

        Note that this is one long command, though we have separated
        it onto two lines using a back-slash.

             find FILE_SYSTEM_NAMES -xdev -type f -user root \
                    -name rdist -perm -04000 -print -ok chmod u-s '{}' \;

        This command will find all files on a system that are
            - only in the file system you name (FILE_SYSTEM_NAMES -xdev)
            - regular files (-type f)
            - owned by root (-user root)
            - named rdist
            - setuid (-perm -04000)

        Once found, those files will
            - have their names printed (-print)
            - have the setuid mode removed, but only if you type `y'
              in response to the prompt (-ok chmod u-s '{}' \;)

     B. Obtain and install the appropriate patch

        Below is a list of vendors who have provided information.
        Details are in Appendix A of this advisory. We will update the
        advisory as we receive more information.

             Berkeley Software Design, Inc.
             Data General Corporation
             Digital Equipment Corporation
             FreeBSD, Inc.
             Hewlett-Packard Company
             IBM Corporation
             Linux
             NEC Corporation
             The Santa Cruz Operation
             Sequent Computer Systems
             Silicon Graphics, Inc.
             Sun Microsystems, Inc.

        If your vendor's name is not on this list, please contact the
        vendor directly.


     C.  If you need the functionality that rdist provides but a patched
         version is not yet available from your vendor, consider installing
         rdist-6.1.3, which is freely available from

              ftp://usc.edu/pub/rdist/rdist-6.1.3.tar.gz

              MD5 (rdist-6.1.3.tar.gz) = 8a76b880b023c5e648b7cb77b9608b9f

         The README file in the distribution explains how to configure
         and install this version of rdist.

         We strongly recommend that you configure this version of rdist to use
         rsh instead of rcmd. Here is the relevant text from the README:

            By default rdist uses rsh(1c) to make connections to remote
            hosts. This has the advantage that rdist does not need to be
            setuid to "root". This eliminates most potential security
            holes. It has the disadvantage that it takes slightly more time
            for rdist to connect to a remote host due to the added overhead
            of doing a fork() and then running the rsh(1c) command.

         For versions of rdist V6 prior to 6.1.3: If you compile rdist with
         -DDIRECT_RCMD *and* you run rdist setuid to root, you are vulnerable
         to the problem described in this advisory. You need to update to
         rdist version 6.1.3.

         Note that by default, rdist V6 is distributed to compile without
         -DDIRECT_RCMD and not run setuid to root. If you have previously
         built version 6.X of rdist with the -DDIRECT_RCMD directive added to
         the $(DEFS_LOCAL) and set "RDIST_MODE = 4555" in "Makefile.local", we
         recommend that you first disable this version with the find command
         given in Section III.A above, then either rebuild rdist with its
         default settings or upgrade to 6.1.3.


.........................................................................
Appendix A: Vendor Information

Below is information we have received from vendors concerning the
vulnerability described in this advisory. If you do not see your vendor's
name, please contact the vendor directly for information.


Berkeley Software Design, Inc.
==============================

        BSD/OS is vulnerable to this problem.

        BSDI has released a patch for rdist in BSD/OS V2.1.

        Sites using the non-kerberized rdist should install patch
        U210-018, which is available from the patches@bsdi.com
        mailback server and also from:

            ftp://ftp.bsdi.com/bsdi/patches/patches-2.1/U210-018

        md5 checksum: 86005d8bbb67eb737120741bd254d26a U210-018

        Domestic licensees that are using the Kerberos package
        should install patch D210-018 from the patches@bsdi.com
        mailback server (this patch is available only to domestic
        licensees because of US export restrictions on crypto
        software). Be sure to install only the appropriate patch.

        md5 checksum: b2060ec4eb9b18ace4e76bcb9441353f D210-018

Data General Corporation
========================

        Data General does not provide a version of rdist as part of the
        standard release of DG/UX. Rdist is available as contributed
        software which is not supported by Data General. This problem will
        be fixed in the next release of the contributed software package.


Digital Equipment Corporation
=============================
#96.0329A

SOURCE:

   Digital Equipment Corporation
   Software Security Response Team

   Copyright (c) Digital Equipment Corporation 1996. All rights reserved.


SUMMARY PATCH-ID INFORMATION:
         The  rdist patch-id's identified in this advisory will not be
         applicable to versions previous to those identified in the
         OP/SYS identified for each patch.
*****
NOTE*  (1) These patch's must be applied if an update or installation
*****      is performed thru V3.2c of Digital UNIX
       (2) The solutions have been included in releases of Digital UNIX
           after V3.2c

TITLES:

        OSF_V2_0     (Patch ID: OSFV20-244-1>
        OSF_V3_0     (Patch ID: OSF300-242)
        OSF_V3_0B    (Patch ID: OSF305-300242)
        OSF_V3_2     (Patch ID: OSF320-184)
        OSF_V3_2B    (Patch ID: OSF325-320184)
        OSF_V3_2C    (Patch ID: OSF350-061)


PATCH AVAILABILITY:
         Software service contract or warranty customers may obtain
         the patched rdist through normal Digital support channels, via
         AES (Advanced Electronic Services). These patch's are available
         from the remedial patch stream for the versions identified
         above and may be found in the applicable versions readme
         files.

                ftp://ftp.service.digital.com/public/osf


         Please refer to applicable Release Note information prior
         to upgrading your installation.

  Note:  Non-contract/non-warranty customers should contact
         local Digital support channels for information
         regarding these patch's.


         As always, Digital urges you to periodically review your
         system management and security procedures. Digital will
         continue to review and enhance the security features of its
         products and work with customers to maintain and improve the
         security and integrity of their systems.

7/23/96                                  - DIGITAL EQUIPMENT CORPORATION


FreeBSD, Inc.
=============

        Versions affected:
                FreeBSD 2.0, 2.0.5, 2.1, 2.1-stable, and 2.2-current
        Versions corrected:
                2.1-stable and 2.2-current as of 1996-07-11
        Workaround:
                As root, execute the commands:

                        # chflags noschg /usr/bin/rdist
                        # chmod u-s,go-rx /usr/bin/rdist

        Patches:
                ftp://freebsd.org/pub/CERT/patches/SA-96:16/

        For more information:
                ftp://freebsd.org/pub/CERT/advisories/SA-96:16/


Hewlett-Packard Company
=======================

   The rdist vulnerability can be eliminated from releases 10.0,
   10.01, 10.10, and 10.20 of HP-UX by applying the patches mentioned
   below.  HP-UX releases prior to 10.X and after 10.20 (i.e., 10.30)
   are not vulnerable.  HP/UX 9.X is not vulnerable.

   Apply patches PHNE_8107 (series 700/800, HP-UX 10.00 & 10.01)
             and PHCO_7798 (series 700/800, HP_UX 10.00 & 10.01)
        or patch PHNE_7919 (series 700/800, HP-UX 10.10)
                 PHNE_7920 (series 700/800, HP-UX 10.20)

   All patches are available now, except PHNE_7920 which
   will be available after 8 August.

   See HEWLETT-PACKARD SECURITY BULLETIN: HPSBUX9608-036, 08 Aug 96
   for more details.


IBM Corporation
===============

   AIX is vulnerable to this problem. Fixes are in process but are
   not yet available. The APAR numbers for the fixes are given below.
   In the meantime, we recommend removing the setuid bit from the
   /usr/bin/rdist program.

  To remove the setuid bit, follow these instructions.

     As the root user, type:

        chmod u-s /usr/bin/rdist

  AIX 3.2
  -------
    Apply the following fix to your system:

        APAR - IX59741

  AIX 4.1
  -------
    Apply the following fix to your system:

        APAR - IX59742

    To determine if you have this APAR on your system, run the following
    command:

       instfix -ik IX59742

  AIX 4.2
  -------
    Apply the following fix to your system:

        APAR - IX59743

    To determine if you have this APAR on your system, run the following
    command:

       instfix -ik IX59743

  To Order
  --------
    APARs may be ordered using FixDist or from the IBM Support Center.
    For more information on FixDist, reference URL:

	  http://service.software.ibm.com/aixsupport/

    or send e-mail to aixserv@austin.ibm.com with a subject of "FixDist".

  IBM and AIX are registered trademarks of International Business Machines
  Corporation.


Linux
=====

    Almost all Linux distributions ship with rdist non setuid.
    If you have changed your rdist to run setuid you are probably
    vulnerable.


NEC Corporation
===============

    EWS-UX/V(Rel4.2)     not vulnerable

    EWS-UX/V(Rel4.2MP)   not vulnerable

    UP-UX/V(Rel4.2MP)    not vulnerable

    UX/4800              not vulnerable


The Santa Cruz Operation
========================

    The following releases of SCO Software are known to contain a version of
    rdist that is vulnerable:

    SCO OpenServer 5.0.2, 5.0.0
    SCO Internet FastStart 1.0

    SCO Open Server Enterprise/Network System 2.0, 3.0
    SCO Open Desktop 2.0, 3.0
    SCO Open Desktop Lite 3.0

    SCO UnixWare 2.0, 2.1

    SCO TCP/IP 1.2.0, 1.2.1

    Patches are being developed for the following releases:

    SCO OpenServer 5.0.2, 5.0.0
    SCO Internet FastStart 1.0
    SCO UnixWare 2.1


Sequent Computer Systems
========================

    Sequent systems do not ship with rdist.


Silicon Graphics, Inc.
======================

    All SGI IRIX versions of rdist are not vulnerable.
    No action is required.

    (When using the find command on SGI IRIX systems, use -mount
    instead of - -xdev. The latter is not supported on SGI IRIX
    systems.)


Sun Microsystems, Inc.
======================

The following patches correct the rdist vulnerability (Sun bug id 1258139),
described in this advisory, on systems running Solaris 1.x or 2.x.

Architecture   SunOS    Solaris   Patch     MD5 checksum for rdist binary
- ---------------------------------------------------------------------------
SPARC          4.1.3     1.1    100383-07  5F2C2B782881FE18D2737B5FA0AEC489
               4.1.3_U1  1.1.1  103823-01  B330358F4E66CD544B9B60AF453C5F2B
               4.1.4     1.1.2  103824-02  419369cc4b3514a9c12b4cdac207fde7
               5.3       2.3    101494-02  1DD34E9E7C50B2C863E30D67DFD1A905
               5.4       2.4    103813-01  90DD81A4C32F7D583737F171B821386B
               5.5       2.5    103815-01  C3BBE3F6758B0BBA7D45CB05009ED80E
               5.5.1     2.5.1  103817-01  89735351119896FEB7469DCA76788561

X86            5.4       2.4    103814-01  EE4509D9CF87DBD29ABB7A72C8330F89
               5.5       2.5    103816-01  3363670F316A06803ECCDD9FFAE95126
               5.5.1     2.5.1  103818-01  8C2E8CFDE7A2AE6D5EC89139D592E71C

PowerPC        5.5.1     2.5.1  103819-01  C3FC0E54B23E4209496A4735D09DFFEF

These patches will be available through your local SunService and SunSoft
Support Services organizations by 9:00 PDT Wednesday, July 24. They will
also be available at the same time from SunSolve Online, via the URL
http://sunsolve1.sun.com.



- ---------------------------------------------------------------------------
The CERT Coordination Center staff thanks Michael Cooper
(Michael.Cooper@Sun.Com) for his work on resolving this problem. He is the
maintainer of the publicly available version of rdist.
- ---------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact
the CERT staff for more information.

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org


Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

CERT is a service mark of Carnegie Mellon University.

This file: ftp://info.cert.org/pub/cert_advisories/CA-96.14.rdist_vul
           http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Nov. 27, 1996  Appendix A, Sun - updated patch information for
			   Solaris 1.1.2, SunOS 4.1.4.
Aug. 30, 1996  Information previously in the README was inserted into the
               advisory.
Aug. 22, 1996  Appendix A, SGI - added note about using the find command.
Aug. 12, 1996  Appendix A, Hewlett-Packard - modified the entry.
July 30, 1996  Solution Section III.A - corrected two misprints in the
	       results of the find command.
July 24, 1996  Appendix A, Digital - added information.
                           IBM - put a new URL in the "To Order" section.











-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMpxYjXVP+x0t4w7BAQGFMAQAyAa73bweFpazYqaSoxSkkRKa20877TPc
9pvT8WKmdp9uEMt6GGQfcnRyWY4z0RaSwmkRFVcfDUMV/wfyQe9M819kPBkn1kLP
y8XOPQTNi7Wmj976N4qtyDy3yXlEb0F+o2OdtM9rVf95iuOy2kGiamZzXCJXn9Hd
0kxARDdJudI=
=t7nV
-----END PGP SIGNATURE-----

