
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT* Advisory CA-97.13
Original issue date: May 7, 1997
Last revised: June 4, 1997 
              Appendix A - updated vendor information for BSDI.

              A complete revision history is at the end of this file.

Topic: Vulnerability in xlock
- -----------------------------------------------------------------------------

The CERT Coordination Center has received reports that a buffer overflow
condition exists in some implementations of xlock. This vulnerability makes it
possible for local users (users with access to an account on the system) to
execute arbitrary programs as a privileged user.

Exploitation information involving this vulnerability has been made publicly
available.

If your system is vulnerable, the CERT/CC team recommends installing a
patch from your vendor. If you are not certain whether your system is
vulnerable or if you know that your system is vulnerable and you cannot add a
patch immediately, we urge you to apply the workaround described in
Section III.B.

We will update this advisory as we receive additional information.
Please check our advisory files regularly for updates that relate to your site.

- -----------------------------------------------------------------------------

I.   Description

     xlock is a program that allows a user to "lock" an X terminal. A buffer
     overflow condition exists in some implementations of xlock. It is
     possible attain unauthorized access to a system by engineering a
     particular environment and calling a vulnerable version of xlock that has
     setuid or setgid bits set. Information about vulnerable versions must be
     obtained from vendors. Some vendor information can be found in Appendix A
     of this advisory.

     Exploitation information involving this vulnerability has been made
     publicly available.

     Note that this problem is different from that discussed in CERT Advisory
     CA-97.11.libXt.


II.  Impact

     Local users are able to execute arbitrary programs as a privileged user
     without authorization.


III. Solution

     Install a patch from your vendor as described in Solution A. If you are
     not certain whether your system is vulnerable or if you know that your
     system is vulnerable and you cannot install a patch immediately, we
     recommend Solution B.

     A.  Obtain and install a patch for this problem.

         Below is a list of vendors who have provided information about
         xlock. Details are in Appendix A of this advisory; we will
         update the appendix as we receive more information. If your
         vendor's name is not on this list, the CERT/CC did not hear from
         that vendor. Please contact your vendor directly.

         Berkeley Software Design, Inc. (BSDI)
         Cray Research - A Silicon Graphics Company
         Data General Corporation
         Digital Equipment Corporation
         FreeBSD, Inc.
         Hewlett-Packard Company
         IBM Corporation
         LINUX
         NEC Corporation
         The Open Group [This group distributes the publicly available software
                         that was formerly distributed by X Consortium]
         Solbourne
         Sun Microsystems, Inc.


     B.  We recommend the following workaround if you are not certain
         whether your system is vulnerable or if you know that your system
         is vulnerable and you cannot install a patch immediately.

         1. Find and disable any copies of xlock that exist on your system and
            that have the setuid or setgid bits set.

         2. Install a version of xlock known to be immune to this
            vulnerablility. One such supported tool is xlockmore. The latest
            version of this tool is 4.02, and you should ensure that this is
            the version you are using. This utility can be obtained from the
            following site:

               ftp://ftp.x.org/contrib/applications/xlockmore-4.02.tar.gz
               MD5 (xlockmore-4.02.tar.gz) = c158e6b4b99b3cff4b52b39219dbfe0e

            You can also obtain this version from mirror sites. A list of
            these sites will be displayed if you are not able to access the
            above archive due to load.

...........................................................................

Appendix A - Vendor Information

Below is a list of the vendors who have provided information for this
advisory. We will update this appendix as we receive additional information.
If you do not see your vendor's name, the CERT/CC did not hear from that
vendor. Please contact the vendor directly.

Berkeley Software Design, Inc. (BSDI)
=====================================

BSD/OS 2.1 is vulnerable to the xlock vulnerability. BSDI recommends that 2.1 
customers either upgrade to BSD/OS 3.0 or remove the setuid permission from 
/usr/X11/bin/xlock.


Cray Research - A Silicon Graphics Company
==========================================
Cray Research does not include xlock in its X Window releases, so we are
not at risk on the xlock buffer overflow problem.


Data General Corporation
========================
The xlock sources (xlockmore-3.7) that DG includes in its contributed
software package have been modified to remove this vulnerability. These
will be available when release 8 comes out. We also recommend that our
customers who have the current version should change the sprintf calls in
resource.c to snprintf calls, rebuild and reinstall the package.


Digital Equipment Corporation
=============================
This reported problem is not present for Digital's ULTRIX or
Digital UNIX Operating Systems Software.


FreeBSD, Inc.
=============
The xlockmore version we ship in our ports collection is vulnerable
in all shipped releases. The port in FreeBSD-current is fixed.
Solution is to install the latest xlockmore version (4.02).


Hewlett-Packard Company
=======================
We ship an suid root program vuelock that is based on xlock.
It does have the vulnerability.

The only workaround is to remove the executable, the patch is "in process".


IBM Corporation
===============
AIX is vulnerable to the conditions described in this advisory.
The following APARs will be released soon:

     AIX 3.2:  APAR IX68189
     AIX 4.1:  APAR IX68190
     AIX 4.2:  APAR IX68191

IBM and AIX are registered trademarks of International Business Machines
Corporation.


LINUX
=====
Red Hat:
        Not vulnerable

Caldera:
        Not vulnerable

Debian:
        An updated package is on the Debian site

SuSE:
        ftp://ftp.suse.com/pub/SuSE-Linux/suse_update/S.u.S.E.-4.4.1/xap1/xlock

And in general the new Xlockmore release fixes the problems.


NEC Corporation
===============
UX/4800              Not vulnerable for all versions.
EWS-UX/V(Rel4.2MP)   Not vulnerable for all versions.
EWS-UX/V(Rel4.2)     Not vulnerable for all versions.
UP-UX/V(Rel4.2MP)    Not vulnerable for all versions.


The Open Group
==============
Publicly available software that was formerly distributed by the X Consortium -

Not vulnerable.


Solbourne
=========
Solbourne is not vulnerable to this attack.


Sun Microsystems, Inc.
======================
We are producing patches for OpenWindows 3.0 for Sun OS versions
4.1.3_U1, 4.1.4, 5.3, 5.4, 5.5, and 5.5.1.



- -----------------------------------------------------------------------------
The CERT Coordination Center thanks David Hedley for reporting the original
problem and Kaleb Keithley at The Open Group for his support in the
development of this advisory.
- -----------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident Response
and Security Teams (see http://www.first.org/team-info/).


CERT/CC Contact Information
- ----------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST(GMT-5) / EDT(GMT-4)
                and are on call for emergencies during other hours.

Fax      +1 412-268-6989

Postal address
         CERT Coordination Center
         Software Engineering Institute
         Carnegie Mellon University
         Pittsburgh PA 15213-3890
         USA

Using encryption
   We strongly urge you to encrypt sensitive information sent by email. We can
   support a shared DES key or PGP. Contact the CERT/CC for more information.
   Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

Getting security information
   CERT publications and other security information are available from
        http://www.cert.org/
        ftp://info.cert.org/pub/

   CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

   To be added to our mailing list for advisories and bulletins, send
   email to
        cert-advisory-request@cert.org
   In the subject line, type
        SUBSCRIBE  your-email-address

- ---------------------------------------------------------------------------
* Registered U.S. Patent and Trademark Office.

Copyright 1997 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

The CERT Coordination Center is part of the Software Engineering Institute
(SEI). The SEI is sponsored by the U.S. Department of Defense.
- ---------------------------------------------------------------------------

This file: ftp://info.cert.org/pub/cert_advisories/CA-97.13.xlock
           http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

June 4, 1997    Appendix A - updated vendor information for BSDI.


-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBM5WRfnVP+x0t4w7BAQECtgP/c9ppI+G2pjiQ5pgPMLqqmhW06qAX7bNC
/qxRHLeYrJW2vooGqgBFBB4rzkmshgzPWoIYXF10KJragRhxxqAhtMUV4VnrkYec
Wz/iI2qY99rKRhC6C+hFM+6rrvkBU8keV/bo3bMCh1oYWQ2uL+0SWddq774a9iOy
IWn+SPYPc/s=
=X42j
-----END PGP SIGNATURE-----

