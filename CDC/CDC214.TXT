
                                    _
                                   | \
                                   |  \
                                   | | \
                            __     | |\ \             __
      _____________       _/_/     | | \ \          _/_/     _____________
     |  ___________     _/_/       | |  \ \       _/_/       ___________  |
     | |              _/_/_____    | |   > >    _/_/_____               | |
     | |             /________/    | |  / /    /________/               | |
     | |                           | | / /                              | |
     | |                           | |/ /                               | |
     | |                           | | /                                | |
     | |                           |  /                                 | |
     | |                           |_/                                  | |
     | |                                                                | |
     | |      c   o   m   m   u   n   i   c   a   t   i   o   n   s     | |
     | |________________________________________________________________| |
     |____________________________________________________________________|

  ...presents...              How to Break the Law
                                                         by Anonymous

                      >>> a cDc publication.......1993 <<<
                        -cDc- CULT OF THE DEAD COW -cDc-
  ____       _     ____       _       ____       _     ____       _       ____
 |____digital_media____digital_culture____digital_media____digital_culture____|

     For a fair number of readers, the day may come when the men in the funny
suits walk up to you, ask if you are you, and then exercise their power of
arrest.  For those without much experience in getting arrested, let me tell you
what in general it will be like (details may vary).

     But first let's review arrest etiquette.  Arrest etiquette can be
complicated for the arresting officers but it is easy for the arrestee.  There
are only two rules: 1) keep your mouth shut and 2) cooperate physically with
the arrest.  Following rule two will help preserve your kidneys, limbs, and
skull but following rule one is the most important.

     During the first two years after your arrest, there are only four words
that you should speak to minions of the State in an official capacity:

     "I want a lawyer"

     Say nothing else.  You gain NO benefits by saying things to the cops and
the prosecutors for free.  If your lawyer cuts a deal for you, you can talk in
exchange for something but once you speak you can't take the words back.
Lawyers are constantly amazed and entertained by the things their clients tell
the cops.  Don't say anything.  It's stupid.

     In _The Hacker Crackdown - Law and Disorder on the Electronic Frontier_;
Bruce Sterling's account of the Legion of Doom/911 Document/Steve Jackson Games
busts of 1990, he reports that all the busted hackers had no clue about how to
be arrested.  They could reprogram telco switches with ease but they didn't
know the basic rules of how to hack the criminal justice system so watch it.

     Once you are placed under arrest, you will probably be handcuffed and
transported to a booking facility where you will be photographed,
fingerprinted, and perhaps given a free set of clothing (these days, usually in
orange).  Then you will be put in a temporary holding cell with many other
interesting people.  Your fellow residents will ask you why you've been
arrested, and when you tell them that you've taken a fall for operating an
outlaw BBS, they'll probably laugh.  Eventually, you'll be brought up before a
judge or magistrate who will set your bail.  If you don't make bail you will be
sent to a longer term facility, perhaps a city prison, to await trial.

     While you're waiting, you can review all the things that you should have
done before breaking the law.  Most free thinkers can come up with a lot of
reasons for violating the law at one time or another.  Just breathing is often
sufficient.  Like any other semi-hazardous activity, careful preparation is the
key.  The purpose of this article is *not* to tell you how not to get caught -
such strategies vary with the laws violated.  No, this article is aimed at
providing you with an outline of how to prepare for getting caught.  No matter
how careful you are in the planning of your criminal career, things can go
wrong.  Thinking about the possibility of failure in advance may encourage you
to improve your over-all strategy and at least will diminish the damage that
the authorities can do to you.

     Psychological preparation is absolutely the most vital task that you must
undertake.  Before you decide to break the law you must have convinced yourself
that the State is wrong and that, for you, the risk of punishment is less
significant than the benefits from violating the law.  There often is a good
foundation for this undertaking, as many people believe that most States are
without much moral justification and amount to criminal gangs in themselves.
So, at least on a philosophical level, breaking the law might be fairly easy
for some of us to do at one time or another.  But it is important to realize
that when you break the law you are playing it for real, and you might have to
face the music.  So, while it is obviously better not to be apprehended in the
first place, defending yourself psychologically if you do get caught will prove
significant, since if the State cannot touch your soul, it gains little by
arresting you.  With proper contemplation you can build on this knowledge and
provide yourself with the best defense against State aggression.

     The philosophical advantage held by those of unconventional mind can be
contrasted with the situation of some conservative tax rebels whose radicalism
is undercut by their belief in the basic legitimacy of the State.  I've seen
conservatives break down when the courts predictably tossed out their
constitutional arguments in tax cases and sentenced them to prison.  Their
background of basic support for the State apparatus lays a heavy layer of guilt
on them once they are labeled criminals.  If you intend to break the law be
sure, in advance, that you won't be feeling guilty.  There will be enough
people only too eager to send you on a guilt trip over your activities, without
your adding to it.

     On a practical level there are many steps you can take to lessen the
ability of the State to punish you effectively:

     1) Learn something about the law.  It helps to know, in advance if you
can, when you are violating the law and a little about the court system and the
possible penalties.  This is important for you, even if you don't intend to
violate the law, since there are enough laws around to allow the State to get
you almost any time it wants on one charge or another.

     There are loads of legal self-help books out there today.  Berkeley's Nolo
Press and the ACLU have published books covering business and personal legal
problems and the rights of various sorts of people (students, mental patients,
gays, etc.).  If you have the time, energy, and money, going to law school
might be a good idea.  This is particularly true in California where loose
eligibility requirements for the State Bar examination have encouraged the
proliferation of "free enterprise" law schools and where it is fairly easy to
get a legal education in your spare time.

     2) Protect your assets.  Apart from imprisonment, violations of the law
may result in fines.  In addition, your activities may lead to civil penalties.
In tax law violations, your property is obviously in jeopardy.  In fact, with
many popular forms of "criminal" activity, your money is in greater danger than
your liberty.

     The recent expansion of the seizure laws has filled the pages of USA Today
and the big-city dailies with page-long lists of bank accounts and other
property seized by law enforcement agents.  This represents a civil liberties
problem of immense proportions, not to mention a practical problem for would-be
law violators.

     The best way to protect your property from loss is to hide it where the
State cannot find it.  Secrecy is your greatest asset.  What the State cannot
find, it cannot confiscate.  You should sell your major personal possessions.
If you own your own home or an expensive late-model car, you risk losing these
possessions if you are convicted of a crime.  Transferring these assets to
friends or relatives is not usually good enough.  If the party trying to
collect money from you can prove that they were given away for less than their
real value with intent to hide them from creditors, the transactions can be set
aside as fraudulent transfers.

     Keep some of your wealth in some anonymous, easily concealable form, such
as cash or gold and silver coins.  If you feel that you must keep bank
accounts, you must arrange it so that no one knows of their existence or can
connect them with you.  In some cases, even Swiss Bank accounts can be attached
by the US authorities if you are convicted of a crime.  If you have interest
earning accounts in your name in this country, the bank will report the
interest earned to the IRS every year (and thus the existence of your account).

     What you should do with any bank account in this country is to set it up
under a nom de guerre.  Even though this is harder than it used to be, it is
still possible.  However you choose to set up your account, you must arrange it
so that statements are not sent to you at your ordinary address.  You can
request that the bank hold statements for you at the bank itself, or you can
use a mail drop of some sort.  If you let any evidence of the accounts
existence come to you through the mails, you may lose the account if the
government opens your mail.  The same problem is encountered with securities or
other types of "paper" investments - they tend to generate a lot of mail.  And
before you risk them, you should make some arrangements to cut off this paper
flow.

     Of course, you may want to keep a small bank account to pay your day-to-
day expenses, but you should only deposit an amount you can afford to lose if
the account is attached.  But be careful not to leave a paper trail connecting
this account with any others you might have.  It is probably safest not to keep
any significant assets in domestic banks at all.  The risks are great, and
thanks to the Federal Reserve Board's Open Markets Committee, the benefit of a
$US account in a US bank is slight.  Foreign bank accounts in strong currencies
are another matter.  I refer you to Harry Brown's _Complete Guide to Swiss
Banks_ for a good discussion of bank secrecy in general and foreign bank
accounts in particular.

     You must avoid investments, such as land, which are on public record and
are difficult to hide.  Highly liquid investments that are easy to hide are
better.  Precious metals are a good idea, and you may want to keep a little
cash around in case you decide to avoid arrest by fleeing.  You can think of
different ways of hiding these assets, but remember that the same consid-
erations which apply to bank accounts apply to any safe deposit boxes in which
you might want to store valuables.

     You should live in a rented dwelling.  You don't want your home to be in
jeopardy while you are sparring with the government.  Car leasing is easy to
arrange these days.  If you don't own these major assets, the government cannot
take them.  You could also drive an older car.  It won't be worth seizing or
won't be much of a loss if it is.

     Your random personal property is generally safe from attachment, but any
valuable collections of books or art will be in jeopardy.  So you should
liquidate them or take steps to protect them.  You can discern the principle
involved from this brief outline.

     Your major assets are either hidden in liquid form or safely in bank
accounts that only you know about and that can not be traced to you.  You
should not use substantial property that is in your own name.  Rent instead.
Each state has its own laws which set forth how much and what type of property
is exempt from attachment by creditors.  Be aware that *government* creditors
have additional collection powers not available to private creditors.
Investigate the law in your state to find how much of the property in your
possession is safe.  It's usually not very much, so plan accordingly.

     This may seem like a radical change to your life, but it is probably
better to change the nature of your property - even if the change is
inconvenient - than to take the chance of losing it.  Besides, cash, gold,
silver, or foreign bank accounts should probably be part of your investment
program already.

     3) Think about your job.  If you have a conventional job with a
conventional employer you may lose it if you are arrested or convicted.  You
may have an easier time of it if you are self-employed or engaged in some
unconventional activity on a professional basis.  I am not saying that you have
to quit your job, but you should analyze the effect that a run-in with the law
will have on your occupation.  It will at least encourage you to think about
alternative means of earning a living, one that will not suffer if you are
arrested.

     It is through our jobs that we are controlled.  Reluctance to change jobs
keeps us in one place, when it might be safer to leave.  Lots of social
regulation has been piled on the employer/employee relationship.  It is there
that most of the taxes we pay are collected.

     Studies of the effect of criminal conviction on income have shown that the
average blue-collar worker regains the same wage earned before imprisonment
within one year after release.  On the other hand, imprisonment dramatically
reduces the wages of white-collar workers, whose jobs are more likely to
involve reputation and "credentials".  If you are self-employed, you will be in
better shape, because you are unlikely to fire yourself for "criminal"
activity.

     Fortunately, there have been some major changes in employment arrangements
for employees as well.  There are numerous contract (temp) and consultant
positions available today for any type or level of job experience.  If you have
been the CEO of a Fortune 500 company, there are agencies that will be happy to
place you as a temp CEO.  Insurance professionals, lawyers, middle-managers,
engineers, secretaries, and waiters, all have temp employment agencies that
serve them.  These jobs can be obtained without background checks and other
invasive procedures (urinalysis).

     4) Avoid involving others.  While you are protecting your property, you
should be sure that you are not entangling friends or relatives in your
activities.  Each person should be left to make her or his own decision in
regard to an illegal undertaking.  You must separate your property and your
actions from the property or activities of those close to you, particularly
those with whom you have a sexual relationship.  It is neither fair nor
particularly good for your relations with others to get them involved in your
arrest.  If you are living with other people it is a good idea to avoid
carrying on illegal activities in the home.  If you do your business elsewhere,
the cops will find it more difficult to charge those living in your home with
accomplice liability based on their knowledge of your crimes.  At no time
should you have anyone who is not a completely informed co-conspirator sign any
documents which are involved in your illegal activities.  Don't slip your
spouse the 1040 to sign after you've "plead the Fifth" on it and claimed Bill
Clinton as a dependent.

     You should also sever your financial ties with the uninvolved.  The state
can grab the full balance of joint bank accounts, even if the "innocent"
partner deposited most of the money.  Other forms of joint property may be
safer, but the state can still grab your half and convert the other owner into
a co-tenant with the government.  Keep your money and other property separate.
If you've followed the suggestions in Section 2 above, you will already have
eliminated most entanglements with others, but such involvements are something
to watch out for.  As I also pointed about above, don't transfer your property
to friends or relatives in anticipation of any criminal activities, since the
state can go after it anyway, dragging others into court.

     5) Develop at least a nodding acquaintance with a lawyer (or someone with
as big a mouth).  If you are arrested, it is very comforting to have someone to
call.  Someone on the outside can do more about getting bail together,
reporting your case to Amnesty International, and getting you out, than you can
do from inside.  You might like to get to know a sympathetic lawyer, if you
happen to have one in the neighborhood.  There are even anarchist lawyers.  As
an anarchist law student once said when asked by his friends how an he could be
a lawyer, "My father is a physician, but that doesn't mean that he believes in
disease."  A philosophically-compatible lawyer should be able to give you some
moral support since he should at least understand your attitude towards the law
you violated.

     6) Practice privacy in your daily life.  Most of us are not used to
keeping information about ourselves *to* ourselves.  We regularly fill out
forms, giving loads of personal information about yourself.  Whenevith otheryou o-ten
parW the geu tell them tbe tout).  If you break thasehiabnts in advancr
fe nyed, yo'ill be in goodpPractice to ive convindingl, when you nyeg it. Ie
can't give youae full courss in privacyotehntiqust in this articln, butThere
areae iew pointrsy:

    a)o Have ell your mail belvtered to a mail recgiving
sericee.  Thero is n

     nyeg for andone out your friends to know where yousfleip.  "MySxistr

     Sm's"  R beca S chrefer
might be  live today ifaf psychy fon hidn'e loske

     upther addresn in the ilies of the CaliforniaDMVf.  Whenevit andoneou s

    
your addres,t give them tbe mail drop addres,tuosing tht bor number as n

    a
paryment numbed.

    b) Uuse aavocbe mai bor for recgiving phone  elns from strangesm.  Thes

    -comlutr- based
sericels are available for about$10/ moith almost ever-

     where in theUSe.  You recgied what lossm like anformlg phone numbed.  You
    -can record e grettingimes agm.  These
sericels are almost uniestinulis-

    lable from g phonetline with aswveringimathier, but thys can be obtaine

     without giving theavocbe mai0 companf any information about yo, an,k of
    -course,tThero is no gtograptic hink
bewehen you and the accound.

    c) Gset yourtutility
sericee in a photy orhborlowed name. PpublicUutilicies
    lare legalty requirde to give you sericee.  Thty may requirs a depositiof
     they don't know you, but thag's a small price topday for privace.  You day
     nyeg some greatd IDt to spart
sericee inbsurauceratce placsm liky
     Californie, but it still can be
dond.

    d) Encryput the personal ilies and recorse on your-comlutr'is haro drivs
    land elopties.  There are manyhmig-cquility,"free ecrypution prograsl out
     in the wolde today.  So you have noexscues.  Don't depend on the ecrypu-

    tionotehnhologves guilt into prograsl like Lo-te1-2-3y orPKZIPe, becausf
     theyouse easil- brakenicither tehntiqusd.

    e5) isrpose of your paper recorss.  almost everyone wh'se convicted of 

    ccrimo is convicted by their own recorss.

    f)  Don't give the government information about yourself. Nainty-frivs
    apeecent of what they know about you sy based on things you've hold thems
    Eeven if you want to follos the better of the la  in these mtiters, watc

    
yourbsurauceratce ilhingl.  Somr of thms are not requirde and othebs carry
     oa practical penalties. Fiew people have
donemanyhgard rimo forecesues
     ressntancs.

    g4) Avoid domestic credis carss. Ggovernment investigaores can do credi

    cchecksion youbyr-comlutr  withoutia  arratd.  Thelresn information in your
    ccredis reports te 
better.  The best way to secuer your financial privac

    tsn to have an of- shrec credis car).  If you must
use a domestic crediu
    -crd,t
use a secuedc credis card that can be obtained without giving ltes
     of personal informatiod.

     lf, after yoe have
done everething you can to protect your mind and your property from the hazares of the criminal justice syste,f you are arrestep
anyway,iag's not the eds of the wolds.  At least you are recgiving
some personalmtitention from nd othewcisecgold anddssntate government.  It is sorr
fe a compiyment cntuallt.  Nst everyone is forth
arrestine.  Most of those
arresteu aron't forth prosecutine. Aned most of toshe prosecurde and convictee
aron't forth imprisohing iother. Iin thisvere of rimnts, governmenes can onle
afford to prosecurs and punishaf rimnwed number of people.

     The government criminal justice enterprise is muchlresneufficienr thanMcDsonadsh, so chancls are they fill fefer you some sorr of deae.  Thty don't
want to ependtions ofdrough oe put you  way and$640 to$100n thos and a year to
keeo you terre. Wwatch toshe deasn thouge.  i chrl  ilk in pleawed guiltd andgnot a sentench as rong s the would have gtiten if
found guiltd it trial. Som rims,iag's
better to take the triau, particularls in oiliccgal
prosecutions.

     lh you und uptavting to epend an ethendedxperods of rimo as agquestoif the
governmenm, you shouldntry to take as relxrde ar attitude as possible.  It day-help to think of the prisoh experience as a wel- earnedivapation. Aafterual,w
yo'ill fiually get the chance to redw all toshe books you'vepcut off redhing over the yeare.  You will asto get thos anns ofdrollais forth of sericelw
yeaely,vinlulding clothins, mnals, odghins, entertainmenm,_medcnal care (sorr
f)l, and educatios.

     Most significanlty ofual,, you will gain
firs-h and experiencsd that cay-help your philosophical andilieorarydDevelonment. Mmanyf amois waitert ddhe
godsuose of their prisohtrime. Aast,, you will have theoupporpuntry to live ie
atotuilitarcan sociahistsState.  in thisdway andange ig's
b codinghgard to find
lvting exaplies of otuilitarcan sociahist governmenee. Ae iew years in prisow
will encourage you tocreoubler youreaffonts tofmight
such sociae systens. ________ __________________________________________________________________
/  _  _ \|De mo RoEachUndrgrnd.806/794-4362|Ktinndom ofSwhi.....806/794-1842 |
((___)) |Choo  B as!..........510/THE-COOL|Polka AE {PW:KILL}..806/794-4362||
[ x x ] |Ripco................312/528-5020|Mgodye Lntertw/Guons415/221-8608 |
  \  /  | TheWwors............617/861-8976|Fiuntopia...........916/673-8412||
 (' ')  |LtunaticLabs.........213/655-0691|ftpt -ftp.eaf.orng in pu/cud/cdc |
  (U)   |================================================================== |
 .ooM   |.1993 cDc compunucatios  by Anonymou                 03/01/93-#214 |\________|AillRrightsDrhoonedAwday.                SIX GLORIO USYEAIRS of Dc |
 