Chaos Digest               Lundi 1 Mars 1993          Volume 1 : Numero 12

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.12 (1 Mars 1993)
File 1--Re: Des adolescents anglais transformes en hackers
File 2--Piratage sur le reseau Janet
File 3--Hackers en Coree du Sud
File 4--Guide d'Utilisation d'un Outil de Securite PC (manuel)

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost from jbcondat@attmail.com. The editors may be
contacted by voice (+33 1 47874083), fax (+33 1 47877070) or S-mail at:
Jean-Bernard Condat, Chaos Computer Club France [CCCF], 47 rue des Rosiers,
93400 St-Ouen, France

Issues of Chaos-D can also be found on some French BBS. Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * ftp.eff.org (192.88.144.4) in /pub/cud
        * red.css.itd.umich.edu (141.211.182.91) in /cud
        * halcyon.com (192.135.191.2) in /pub/mirror/cud
        * ftp.ee.mu.oz.au (128.250.77.2) in /pub/text/CuD
        * nic.funet.fi (128.214.6.100) in /pub/doc/cud

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Tue Feb 23 20:06:14 CST 1993
From: jp-sorlat@altern.com (jp-sorlat )
Subject: File 1--Re: Des adolescents anglais transformes en hackers


NEWS RELEASE
Press Officer: Martin Herrema: 071 911 5101

February 24, 1993
Statement on Hacking at PCL


The Polytechnic of Central London (now the University of Westminster) was
affected by a 'hacker' on April 23, 1990.  The 'hacker' gained access to
the polytechnic's computer teaching systems via the academic joint computer
network JANET, which serves all British universities.

The 'hacker' made some changes to the system, mainly to the on-screen
start-up messages.  These were spotted very quickly on the morning of April
23 by staff, who then changeg the public access messages back to the
correct wording.  The University of London Computer Centre, which provides
network support for JANET, was informed of the situation.

The teaching systems contained a variety of information concerned with
course administration and delivery, but there was no evidence thet the
hacker achieved anything other than a change of message.  The systems were
turned completely to normal within a few days, and the incident caused
minimal disruption to the work of the polytechnic.  Since the incident
further security measures have been built into the University's computer
systems.

------------------------------

Date: Wed Feb 24 13:19:23  1993
From: S15810@PRIME-A.PLYMOUTH.AC.UK (Stuart Wyatt )
Subject: File 2--Piratage sur le reseau Janet


                     ENGLISH HACKER PROSECUTED
                     +++++++++++++++++++++++++

Unlike America and most other countries, England does not have too much to
show for arresting hackers and bringing them to justice. This means that
each time a hacker is caught, it makes headline news and creates the same
foreboding atmosphere that hacking reports created in the early 1980's.
The latest story is of a young hacker, Paul Bedworth.

Bedworth started hacking at the age of 17, and through the many computers
he accessed, he teamed up with 2 other hackers 10 years his senior. From
his bedroom, using his #200 computer he dialed into pad-ports at
Universities and from there, penetrated Internet. By spending hours at a
time hacking, he amassed huge telephone bills (one was 34 pages long). Due
to the BT bills, his parents banned him from using their telephone. This
however did not stop his antics - He soldered a link from the BT socket
and ran the wire under the carpet into his bedroom.

By using Janet (The Joint Academic Network) he hacked into many University
computers with the main intent to delete sensitive files and crash the
system. His idea of hacking was to disrupt as many computer systems as
possible. Other computers he attacked were the Financial Times network,
Lloyds bank, and a whole host of computers spanning France, Germany and
Luxenbourg.

He got around the problem of running up huge telephone bills by accessing
the billing computer, and transfering large segments of the bill to
innocent users at Manchester University.

Bedworth was arrested after another University put a trace on his calls.
He was charged with three dishonesty charges, which he pleaded not guilty
in court. The case is continuing at the moment (24 Feb 1993) and I will
forward any news as and when I get it.

In the U.K., there is a law which prohibits hacking, and hackers can be
charged with gaining unlawful access to a machine and if any data is
altered or destroyed, then that is also a criminal offence. It seems
strange that Bedworth was not charged with gaining unlawful access and
the destruction of data - But then, unlike the rumours that circulate the
world, the British judicial system is not all its cracked up to be.
                                                            ________
-Stuart Wyatt                                              (        )____
                                                          (  Alas, life  )
P.S. I am currently collating information on hacking and(   is but an   )
     hackers for a forthcoming book. If anyone wishes   (    Aardvaark.. )
     to contact me IN THE STRICTEST OF CONFIDENCE then   (       __      )
     feel free to email me.                           .   (_____)  (____)
* * * * * * * * * * * * * * * *                     . ? .       ()
* CHEERS_ THEN - _     _      *                       __      ()
*   ___/_/______|_|___| |__   *                     /    \  ()
*  |________   _______| |__|  *                    |_    _|
*    / /    | | | |   | |     *                   |(0)||(0)|
*   / /___  | | | |   | |     *                  /|_  \/  _|\
*  /___  /  | | | |   | |     *                  || | == | ||
*     / /   | | \  \__/ /     *                  || \____/ ||
*    / /    |_|   \____/      *                 ///\  !!  /\\\
*-*-/_/-*-*-*-*-*-*-*-*-*-*-*-*-=-=-=-=-=-=-=-=-!!!-!-=-=-!-!!!-=-=-=-=-=-=
>From : Stuart Wyatt (Student, HNDCS1)
       Faculty of Technology,         *> Be excellent to everyone - dude <*
       University of Plymouth,
       Drake Circus,                Email: S15810@uk.ac.plym.pa     (PRIME)
       Plymouth, England.                  stuartw@uk.ac.plym.cd.zeus (SUN)
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

------------------------------

Date: 20 Feb 93 06:17:14 GMT
From: kafka@desert.hacktic.nl (Kafka )
Subject: File 3--Hackers en Coree du Sud
Repost from: alt.security

+++++
COMPUTER HACKER HELD FOR FORING PAPERS

Seoul, Feb. 18 (AFP) - South Korean prosecuters
have arrested a computer hacker on charges of
falsifying presidential Blue House documents to
demand classified data from a dozen financial
institutions, authorities said today. Prosecuters
arrested Kim Jae-yol, 23 late yesterday in the
first such hacker case here on multiple charges of
forgery of official documents and attempted fraud.
Kim, an unemployed, self-taught computer wizzard,
allegedly falsified the Blue House's secret
computer password and presidential facsimiles to
demand classified computer information from 12
financial institutions, includang several
commercial banks.
+++++

Mmmm, I wonder how you 'falsify (....) a password'.

    (P)

==== Kafka ======= kafka@desert.hacktic.nl ===== 1st class l00zur =========
"The Techno Rebels are, whether we recognize it or not, agents of the Third
      Wave. They will not vanish but multiply in the years ahead."
                  - The Third Wave, Alvan Toffler

------------------------------

Date: Mon Feb  8 08:51:00 CST 1993
From: Cerasela Tanasescu
Subject: File 4--Guide d'Utilisation d'un Outil de Securite PC (manuel)
Copyright: DCSIS SA, 1993


                               KHEOPS
                     The confidentiality of your PC

                        USER GUIDE--Version 2.1


            S.A. DCSIS, Avenue de Cambridge, Technopole CITIS
                  14200 HEROUVILLE SAINT CLAIR, FRANCE
              Phone: (33) 31.06.00.06, Fax :(33) 31.43.79.95


WARNING

The DCSIS Company may not be held responsible for the damage that may be
caused directly or indirectly by this software.


PRECAUTION

Any manual installation or uninstallation together with any illegal copy of
KHEOPS may damage the data on your hard disk.

We strongly recommend performing a backup of your data before installing
KHEOPS. This backup should be renewed frequently as a prevention against
fraudulent intrusion attempts which might affect the right functionning of
KHEOPS.

If this was the case, DCSIS could not be held responsible for any damage.



1. PRESENTATION OF KHEOPS

Nowadays, all micro-computers contain sensitive data: client files,
production figures, programs, payrolls etc... Besides, all this data and
knowhow are stored on computers that more and more people know how to use,
whether they are entitled or not. The risk of information leak and file
copy is growing.  Companies must therefore protect themselves.

DCSIS has developped KHEOPS with this in mind.  This is an access control
software for all PC/AT micro-computers running MS/DOS(Versions 3.2x and
5.0).  This program ensures the safety of the PC whether mono or multi
users, connected to a network  or not, so that maximum confidentiality is
guaranteed.

Main Functions

- computer access control;
- computer locking up when the user gets away from his PC;
- boot control with a diskette.

Besides, with the diskette administrator program:

- users management (up to 31 + administrator)
- logbook consultation
- computer unlocking
- program customization


2. MAIN FUNCTIONS

The security ensured by KHEOPS is strictly software, i.e. there is no need
of any hardware device.

2.1. Access control

Computer security conventions specify two terms: user identifier and
password.  Every user is given: (1) a user identifier, hereafter user id,
which may be structured around the notion of user group, (2) a password.
This pair user id/password must be unique and known only to the user.  The
password must obviously never be written or communicated to anyone.

Procedure

Access control is carried out by the only program that can run when the hard
disk is locked up.  This program prompts the user to type his user id. and
password. Once this is done, the pair user id/password is looked up by
KHEOPS in a user list stored on the disk. This list is coded and is unknown
to the operating system.

If this control is successful and the password is recognized as valid
(validity date), then the hard disk is made available and the user can make
full use of his PC.

On the other hand, if the authentification fails, the user is invited to
start again.  After 'n' unsuccessful attempts, keyboard and hard disk are
locked up: hard disk reboot is impossible. Diskette boot will not give
access to hard disk.  Only the administrator can put things in order again
thanks to his KHEOPS diskette.

Hard disk locking will also occur if a user tries to log in with an outdated
password.

The user list can hold up to 32 entries, that is an administrator plus 31
users.  This makes it possible to share a PC between several users, every
one of them being answerable for his session.


2.2. Hard disk locking

When the disk is locked up, it is apparently empty.  The only response to
the DIR command is: "NO FILE FOUND".  The disk and all its partitions
(virtual disks D:\, U:\, V:\...) are also locked up.  It is impossible to
copy or delete files, to create directories of to execute programs.  Last
but not least, neither NORTON nor PCTOOLS are able to retrieve the files.


2.3. Resident program

When the PC remains unused for a certain period of time, a small resident
program is activated after a given period of time.  This program locks up
screen and keyboard, and displays the standard login window.  The only
action that remains possible is to enter user id. and password.  The
resident program is activated due to a temporization or upon user request,
e.g. by pressing right SHIFT/left SHIFT.


3. SECURITY MANAGEMENT

3.1. User Identifier and Password

Both user id. and password must be 7 characters long.  But contrary to the
user id. which is permanent, the password must be renewed regularly.

Let us explain the procedure: when anyone runs the access control for the
first time, he is invited to enter his password twice (to make sure the
spelling is correct).

This password has a three months validity.  During the last 15 days of
validity the user will be prompted to change his password.  A message will
display the deadline and explain that this change is compulsory.  The user
is free to take no notice of this warning but once the deadline is reached
the computer disk will be locked up and only the security administrator
will have power to straighten things up.

If the user decides to change his password, again KHEOPS will invite him
to type it twice.  The new password will only be accepted if it is found to
be different from the user's last 6 passwords.

The procedure explained above is only valid for "standard users".  There
are two exceptions:

- the administrator for whom there is no password time restriction;

- temporary users.  The validity deadline is decided upon by the
administrator.  These password are not renewable.  This option allows
people to work on the PC on a temporary basis.


3.2. Logbook

Like in the case of the user id/password list, the logbook is coded.  Only
the administrator is entitled to consult it.  This book is updated at the
time of access control.

The following information is available:

- successful connexions (and number of attempts);
- disconnexions and duration of session;
- PC lock up following 'n' unsuccessful password presentations;
- PC lock up due to outdated password presentation.

Every entry in the logbook also includes date, time and user id.  In the
case of a system locked up due to 'n' unsuccessful attempts, only the last
user id. is kept.

The logbook contains up to 127 entries.  Once the file is full, new entries
replace the oldest records.


4. KHEOPS IMPLEMENTATION (Administrator)

4.1. Installation

KHEOPS installation imperatively requires a floppy disk drive.  Use a KHEOPS
diskette with the adequat format (3.5' or 5.25').

- Insert the KHEOPS diskette into drive a;
- Boot the PC again (on a:).

The following pull-down menu will then be displayed on the screen:

                 |        MAIN MENU          |
                 | - Install KHEOPS          |
                 | - Modify present system   |
                 | - Uninstall KHEOPS        |

On every screen the following warning is displayed for information:

                 | On all menu               |
                 | ESC:  quit                |
                 |    :  preceding item      |
                 |    :  next                |
                 |    :  item(un)select      |

- Validate every option by pressing the ENTER key;

- Choose option "INSTALL KHEOPS".  The program will display the following
message:

              "Installation under way. Please wait".

- Another window will pop up:

                 |  Administrator characteristics  |
                 |       User identifier :         |
                 |       Password        :         |

- Enter administrator user id. and password (7 characters).  Both must be
validated by pressing ENTER key.  They will be required on every use of
the KHEOPS diskette;

- Confirm the password.

Installation will then proceed.  Wait for the message:

                       "KHEOPS is installed"

Program customization is now necessary.

               |            SYSTEM CUSTOMIZATION            |
               | Resident program activation delay (mn) : 5 |
               | Background tasks authorized            : 0 |
               | Debugging authorized                   : 0 |
               | New hot-key                            : N |
               | Screen backup drive                    : C |
               | Number of attempts before lock up      : 3 |
               | Windows utilization                    : N |

The administrator can now "add a user".

- Press "ENTER" to type a user id, or
- Press "ESC" if the user list is complete.

The first user that is created in the list is the administrator.

In order to put an end to KHEOPS installation :

- Press "ESC" key on Main Menu;
- Remove the KHEOPS diskette (KHEOPS will prompt you to do so);

PC will reboot automatically.  On PC reboot, the screen will display the
standard login screen.  Two situations may now occur:

Case #1: The administrator attempts to log in. He can type his id. and
password,

Case #2: A new user logs in.  The program then displays the following
message:

                        "Enter new password"

The password must be 7-characters long.  This password must be checked as
typing errors are easily made.  This is why the user is prompted to type
his password once again.

If the two secret codes are identical, access to the PC is authorized.

                   "KHEOPS installation is complete"


4.2. System Modification

- Insert KHEOPS diskette into drive a:.
- Type the following, then press "ENTER":

                              "A:KHEOPS"

A window invites the administrator to type his id. and password.

                 |   Administrator characteristics   |
                 | User identifier :                 |
                 | Password        :                 |

Following which the main pull-down menu is displayed on the screen:

                 |             MAIN MENU             |
                 | - Install KHEOPS                  |
                 | - Modify current system           |
                 | - Uninstall KHEOPS                |

Upon selection of "Modify current system" another pull-down menu is
displayed:

                 |        SYSTEM MODIFICATION        |
                 |  Work on the list                 |
                 |  Unlock hard disk                 |
                 |  Logbook consultation             |
                 |  System customization             |

Work on the list
++++++++++++++++

                 |         WORK ON THE LIST          |
                 |  Cancel a user                    |
                 |  Add a user                       |
                 |  Add a temporary user             |
                 |  Unlock a user                    |
                 |  User list consultation           |

"Add a user"

This option is used to allow a new user to use the micro-computer.  The
administrator is invited to type the user's name:

                 |        User characteristics       |
                 |   Name : Doherty                  |

This name is the user id. and must imperatively be 7-characters long.

"Cancel a user"

This option is used to disable a user.  The administrator is invited to
type the user's name:

                 |           User Removal            |
                 |   Name :                          |

Once the administrator has pressed ENTER, the user no longer stands in the
list.

"Add a temporary user"

This option makes it possible to add a user to those who have access to the
PC on a temporary basis.

                 |          Temporary user           |
                 |   Name :                          |

- Type his user id. and press "ENTER".


The program will then invite the administrator to type the user's time limit
of access to the PC.

                 |         TEMPORARY ACCESS          |
                 |         Validity deadline :       |
                 |  Year     : 91                    |
                 |  Month    : 07                    |
                 |  Day      : 15                    |
                 |                                   |
                 |  Confirmation (Y/N) : o           |

- Type two digits for year, month and day, then validate your data by
pressing "Y"es of "N"o.

"Unlock a user"

When a password is outdated (3 months validity period), the PC is
temporarily locked up.  Only the administrator can unlock it thanks to
this option.

"Display user list"

This option is used to display the list of all users entitled to use the PC.

Unlock Hard Disk
++++++++++++++++

After 'n' unsuccessful login attempts the hard disk is locked up.  Only the
administrator has the possibility to unlock it thanks to this function.

Logbook Consultation
++++++++++++++++++++

A cyclic logbook containing the last 127 entries is displayed with the
following data:

     - user id.;
     - date and time;
     - action: login, logout, PC locking;
     - description: number of login attempts, duration of connexion, number
       of unsuccessful attempts.

This option makes it possible for the administrator to check who uses the
micro-computer and for how long.


System Customization
++++++++++++++++++++

Thanks to this option the administrator can adapt KHEOPS to users need.

              |            SYSTEM CUSTOMIZATION            |
              | Resident program activation delay (mn): 5  |
              | Background tasks authorized           : o  |
              | Debugging authorized                  : o  |
              | New hot-key                           : o  |
              | Screen backup drive                   : c  |
              | Number of attempts before lock up     : 3  |
              | Windows utilization                   : n  |

* "Resident program activation delay"
  The administrator should type the temporization that is desirable before
  the login screen is displayed when the PC remains unused.

* "Background task authorization"
  We strongly recommend using this option in case of intensive use of the
  hard disk (e.g. compilation, printing).  Keyboard will be locked up, but
  background tasks will carry on their job. In case your reply is No,
  keyboard, screen and hard disk will be subject to access control.
  Remark: "Background tasks" must be authorized in the case of a network
  server.

* "Debugging authorized"
  The administrator indicates whether or not the use of a debugging tool is
  authorized.
  Remark: Debugging should be authorized in case the PC shows uncontrollable
  reboot problems.

* "New hot-key"
  The hot-key activates the resident program at any time.  The default hot-
  key value is "left shift/right shift".  Nevertheless the administrator is
  free to change the hot-key value.

* "Screen storage disk"
  This option enables screen backup on a hard or virtual disk (200 KB are
  necessary).  The default disk drive is c:.

* "Number of attempts before system lock-up"
  This function makes it possible to customize the number of user id/
  password presentation attempts before hard disk lock-up.

REMARK: Any modification of one of these options will only be taken into
account once reboot has been performed.

4.3. Uninstalling

- Insert KHEOPS diskette into drive a:;
- Type: "A:KHEOPS";

The following window pops up:

                 |     Administrator characteristics     |
                 |  User identifier :                    |
                 |  Password :                           |

Once the administrator id. and password have been validated and checked
KHEOPS displays the main menu.

                 |               MAIN MENU               |
                 |  - Install KHEOPS                     |
                 |  - Modify present system              |
                 |  - Uninstall KHEOPS                   |

- Choose "Uninstall";

After a few seconds the following message is displayed on the screen:

                        "The system must be reset"

- Take the KHEOPS diskette out of the drive as the PC will reboot
automatically.

                    "KHEOPS has been totally uninstalled"

4.4. Hard Disk Unlocking

After 'n' unsuccessful user id/password presentations, the following
message is displayed on the screen:

                 | Hard disk and keyboard are locked up. |
                 |    Please phone the administrator.    |

The PC will be unable to boot on hard disk.  Any attempt to do so will
result in the screen to display the following message:

                           "Non system disk".

- Insert KHEOPS diskette into drive a:;
- Reset the PC so that it boots on the diskette;
- Type adminitrator id. and password.

The following message is then displayed:

                        "The system must be reset"

- Leave the diskette in drive a: and reboot.  Then, when prompted to, type
administrator id. and password.

                           The PC is unlocked.


5.  KHEOPS IMPLEMENTATION (Users)

Now that KHEOPS has been installed, your data on the PC's hard disk is
protected.

5.1. PC Reset

At boot time, the user is invited to type his id. and password.  Having
done that, he has free access to the resources of the PC.

5.2. Resident Program Activation

The function of the resident program is to lock up screen, if background
tasks are not authorized, and keyboard either due to temporization or upon
user request:

- Temporization: in case the PC has remained unused for 'n' minutes (as
set up by the administrator in the customization session, screen (if
background tasks are not authorized) and keyboard action is automatically
suspended.

KHEOPS displays the standard login window and invites the user to type his
id. and password.  If the same user logs in again, his application is
restaured.  In case another user attempts to connect himself then the PC
reboots.

- User request: the resident program can be deliberately activated by the
user by pressing the hot-key.

KHEOPS then displays the following message:

                      "End of session (Y/N) ?"

* By pressing the letter "Y" the user will ensure that the PC is
  automatically rebooted if another user logs in;

* The user answers "N".  This means that he wants to suspend his job
  temporarily.  KHEOPS displays the following message:

                         "Computer in use"

The only id/password acceptable are those of the user who pressed the hot-
key.  Any other user will be rejected.


6.  KHEOPS COMPATIBILITY

6.1.  Hardware

KHEOPS can be installed on IBM PC/AT, PS/2 or compatible micro-computers
running MS/DOS version 3.2 to 5.0.  DCSIS has tested this program on the
following computers: IBM PC/AT & PS/2, COMPAQ DeskPro (286S and 386S) and
LTE 286, SLT 286 and 386, EPSON EL2 and EL3S, GOUPIL G5 286, HP VECTRA 486,
OLIVETTI PC 310

6.2.  Software

6.2.1. Standard software

KHEOPS has been tested and validated by DCSIS with the following programs:
MICROSOFT C, TURBO C, TURBO PASCAL, PCTOOLS, NORTON, DBASE, LOTUS 1.2.3...

6.2.2. Graphic software

KHEOPS can handle mouse and keyboard, which ensures compatibility with the
following programs: WORD 4 and 5, WINDOWS 2 and 3, PAINTBRUSH, PAGE MAKER

6.2.3. Antivirus software

KHEOPS is compatible with VIRUSAFE+, trademark registered by Eliashim
Microcomputers, which has been recognized as one of the best antivirus
programs on the market (VIRUSAFE+ can handle 1400 virus and is wellknown
for its preventive and curative vocation).  The joint use of KHEOPS and
VIRUSAFE+ ensures maximum micro-computer safety.  Install Virusafe+ first
then KHEOPS (this is imperative).

6.2.4. Network

It is possible to install KHEOPS on every workstation of a local area
network (e.g. NOVELL network) so that every station is protected locally.

------------------------------

End of Chaos Digest #1.12
************************************

Downloaded From P-80 International Information Systems 304-744-2253
